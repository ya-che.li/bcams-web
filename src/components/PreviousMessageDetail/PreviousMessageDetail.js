import React from 'react';
import {Container, Table} from 'semantic-ui-react';
import TextButton from '../shared/TextButton/TextButton';

const PreviousMessageDetailRows = ({messages=[]}) => {
    return messages.map((message, index) => (
        <Table.Row key={`PreviousMessageDetailRows_${index}`}>
            <Table.Cell collapsing>{(new Date(message.date).toDateString())}</Table.Cell>
            <Table.Cell collapsing>{message.email}</Table.Cell>
            <Table.Cell collapsing>{message.firstname}</Table.Cell>
            <Table.Cell collapsing>{message.lastname}</Table.Cell>
            <Table.Cell>{message.medium}</Table.Cell>
        </Table.Row>
    ));
};

const PreviousMessageDetail = ({onBackClick, onResendMessageClick, messages}) => {
    return (
        <div>
            <Container>
                <TextButton text="Back to Previous Message List" onClick={onBackClick} />
                <div className="ui right floated primary button" onClick={onResendMessageClick}>
                    Resend Message
                </div>
                <Table celled striped>
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Date</Table.HeaderCell>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>First Name</Table.HeaderCell>
                        <Table.HeaderCell>Last Name</Table.HeaderCell>
                        <Table.HeaderCell>Medium</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <PreviousMessageDetailRows messages={messages}/>
                    </Table.Body>
                </Table>
            </Container>
        </div>
    );
};

export default PreviousMessageDetail;
