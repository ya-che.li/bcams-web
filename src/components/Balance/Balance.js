import React from 'react';
import {createUseStyles} from 'react-jss';

const useStyles = createUseStyles({
    root: {
        marginTop: "1em"
    }
});

const Balance = ({ currencySymbol, balance }) => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <b>Remaining Balance: </b>
            <label>{currencySymbol}{balance}</label>
        </div>
    );
};

export default Balance;
