import React from 'react';

export default function LabelField({test_id="", label="", children}){
    return <div className="field" test_id={test_id}>
        <div className="sixteen wide field">
            <label>{label}</label>
            {children}
        </div>
    </div>
}
