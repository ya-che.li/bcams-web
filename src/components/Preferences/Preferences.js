import React from 'react';
import ThemeDropdownListContainer from "../../containers/ThemeDropdownListContainer";
import CurrencyDropdownListContainer from '../../containers/CurrencyDropdownListContainer';
import ClubCheckboxListContainer from '../../containers/ClubCheckboxListContainer/ClubCheckboxListContainer';
import TextButton from '../shared/TextButton';
import {CONTACT_EMAIL, CONTACT_SMS, CONTACT_WHATSAPP} from '../../constants/contacts';
import TextInput from '../shared/TextInput/TextInput';
import EmailInput from '../shared/EmailInput/EmailInput';
import ContactDetailsDropdownListContainer from '../../containers/ContactDetailsDropdownListContainer';
import {get_email_disabled_text, get_mobile_disabled_text} from '../../Helpers/Validators';
import LabelField from '../LabelField';

function getContactErrorText({contact_medium_id, contact}){
    switch(contact_medium_id){
        case CONTACT_SMS:
            return get_mobile_disabled_text(contact);
        case CONTACT_WHATSAPP:
            return "";
        case CONTACT_EMAIL:
            return get_email_disabled_text(contact);
        default:
            return "";
    }
}

function isCurrencyShown({isAdmin=false, isComposer=false}){
    return isComposer || isAdmin;
}

function isComposeAlertShown({isAdmin=false, isComposer=false}){
    return isComposer || isAdmin;
}

const Preferences = props => {
  const {
    email,
    theme_id,
    setThemeId,
    contact,
    setContact,
    contact_medium_id,
    setContactMediumId,
    currency_id,
    setCurrencyId,
    userClubs,
    setUserClubs,
    onAlertClick = () => {},
    onAdminClick = () => {},
    onDiscardClick = () => {},
    onSaveClick = () => {},
    isAdmin=false,
    isComposer=false
  } = props;

  const contactErrorText = getContactErrorText({contact_medium_id, contact});
  const isContactEnabled = contactErrorText === "";
  const isThemeSelected = !!theme_id;
  const isSaveEnabled = isContactEnabled && isThemeSelected;

  return (
    <div className="ui padded text container segment">
      <form className="ui form">
        <div className="left-form">
            <LabelField label="Email Address">
                <p>{email}</p>
            </LabelField>
            <LabelField label="Theme">
                <ThemeDropdownListContainer test_id="theme_dropdown" value={theme_id} onChange={setThemeId} />
            </LabelField>
            <LabelField label="Contact Details">
                <ContactDetailsDropdownListContainer value={contact_medium_id} onChange={setContactMediumId} />
                {contact_medium_id === CONTACT_SMS && <TextInput placeholder="contact Number" value={contact} maxLength={255} onChange={setContact} /> }
                {contact_medium_id === CONTACT_EMAIL && <EmailInput placeholder="contact Email" value={contact} maxLength={255} onChange={setContact} /> }
                {contactErrorText!=="" && <p style={{ color: "red" }}>{contactErrorText}</p>}
            </LabelField>
                {isCurrencyShown({isAdmin, isComposer}) &&
                    <LabelField test_id="currency_field" label="Currency"> <CurrencyDropdownListContainer test_id="currency_dropdown" value={currency_id} onChange={setCurrencyId} /></LabelField>
                }
        </div>
        <div className="right-form">
          <ClubCheckboxListContainer values={userClubs} onChange={setUserClubs} />
        </div>
        <div className="inline fields">
          <div className="ten wide field left-buttons">
              {isComposeAlertShown({isAdmin, isComposer}) && <TextButton test_id="alert_button" text="Compose Alert" onClick={onAlertClick} />}
              {isAdmin && <TextButton test_id="admin_button" text="User Admin" onClick={onAdminClick} />}
          </div>
          <div className="six wide field right-buttons">
            <TextButton disabled={!isSaveEnabled} text="Save" onClick={onSaveClick} />
            <TextButton text="Discard" onClick={onDiscardClick} />
          </div>
        </div>
      </form>
    </div>
  );
};

export default Preferences;
