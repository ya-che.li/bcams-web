import React from 'react';
import Checkbox from "../../components/shared/Checkbox";
import Loading from "../../components/shared/Loading";

export default ({
  clubs,
  loading = false,
  values = [],
  onChange,
  is_disabled_all = false
}) => {
  if (loading) {
    return <Loading />;
  } else {
    return (
      <div className="ui checkbox">
        <fieldset>
          <legend>Affected clubs</legend>
          {clubs.map(({ id, name }) => {
            const key = `${id}_${name}`;
            const checked = values.indexOf(id) !== -1;

            return (
              <div className="field" key={key}>
                <Checkbox
                  id={id}
                  disabled={is_disabled_all}
                  checked={checked}
                  onChange={checked => {
                    onChange({ id, checked });
                  }}
                />
                <label htmlFor={id}> {name}</label>
              </div>
            );
          })}
        </fieldset>
      </div>
    );
  }
};
