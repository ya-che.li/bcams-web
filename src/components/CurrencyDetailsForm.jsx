import React from 'react';
import CurrencyDropdown from './CurrencyDropdown';

const CurrencyDetailsForm = ({ currencyId, currencyIdChanged,authToken }) => { 
	const onCurrencyChange = (event, data) => {  
		currencyIdChanged(data.value);
	}

	return (
		<div className="field">
			<label>Currency</label>
			<div className="four wide field">
				<CurrencyDropdown currencyId={currencyId} onChange={onCurrencyChange} authToken={authToken}/>
			</div>

		</div>
	);
};

export default CurrencyDetailsForm;
