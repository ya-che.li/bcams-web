import React from "react";

const Checkbox = props => {
	return <input type="checkbox" {...props} checked={props.selected} />;
};

const ClubCheckbox = ({ id, club, onChange, isDisabled }) => {
	
	return (
		<div className="field">
			<Checkbox
				id={id}
				selected={club.selected}
				disabled={isDisabled}
				onChange={e => {
					onChange({
						id: club.id,
						selected: e.target.checked
					});
				}} />  
				<label htmlFor={id} > {club.name}</label>
		</div>
	)
};

export default ClubCheckbox;
