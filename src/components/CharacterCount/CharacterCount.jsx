import React from "react";

const CharacterCount = ({count, maxTotalCount}) => {
    return (
        <div>
            <p>
                Character count: ({count}/{maxTotalCount})
            </p>
        </div>
    );
};

export default CharacterCount;
