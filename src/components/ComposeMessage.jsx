import React, {useEffect, useState} from "react";
import {Button, Dimmer, Loader} from 'semantic-ui-react';
import Header from "./Header";
import cx from "classnames";
import ClubCheckboxContainer from "../containers/ClubCheckboxContainer";
import Balance from "../components/Balance";
import {Redirect} from "react-router-dom";
import ValidationMessage from "./ValidationMessage/ValidationMessage";
import {useClubFetch} from "../Hooks/Clubs";
import {useBillingFetch} from "../Hooks/Billing";
import {redirection} from './Preferences';
import httpClient from '../Helpers/http-client';
import {withAuth} from '@okta/okta-react';

const sender = "david.johnson@and.digital"; 

const ComposeMessage = ({auth}) => {
	const [email, setEmail] = useState('');  
	const maxTotalCount = 100; 
    const [authToken, setAuthToken] = useState('');   
	const [clubs, setClubs, clubsLoading, clubsError] = useClubFetch(authToken);
	const [balance, balanceLoading, balanceError] = useBillingFetch(email, authToken); 
	const [titleDirty, setTitleDirty] = useState(false);
	const [descriptionDirty, setDescriptionDirty] = useState(false);
	const [priorityDirty, setPriorityDirty] = useState(false);

	const [descriptionValid, setDescriptionValid] = useState(false);
	const [titleValid, setTitleValid] = useState(false);

	const [red, setRed] = useState(false);
	const [blue, setBlue] = useState(false);
	const [priority, setPriority] = useState(false);
	const [title, setTitle] = useState("");
	const [messageDescription, setMessageDescription] = useState("");

	const [confluenceUrl, setConfluenceUrl] = useState(null);
	const [clubSelectionDisabled, setClubSelectionDisabled] = useState(false);
	const [redirect, setRedirect] = useState(false);
	const [titleCount, setTitleCount] = useState(0);
	const [messageCount, setMessageCount] = useState(0);
	const [dimmerActive, setDimmerActive] = useState(false); 
	const [previousRedirect, setPreviousRedirect] = useState(false);
	
	useEffect(() => { 
		auth.getUser().then(user => { 
            setEmail(user.email)
        });
        auth.getAccessToken().then(token=> { setAuthToken(token)});

    }, [auth]);
	const generateMessageToSend = () => {
		const message = {
			sender_id: sender,
			balance: balance,
			header: title,
			priority_id: priority,
			message: messageDescription,
			url: confluenceUrl || ""
		};

		message.affected_club_ids = clubs
			.filter(club => club.selected)
			.map(club => club.id);

		return message;
	}

	const priorityOptions = {
		red: 13,
		info: 14
	};

	var redButton = cx({
		"ui button": true,
		red: red
	});

	var infoButton = cx({
		"ui button": true,
		blue: blue
	});

	const onClickRed = () => {
		setRed(true);
		setBlue(false);

		setPriority(priorityOptions.red);
		setPriorityDirty(true);
	};

	const onClickBlue = () => {
		setBlue(true);
		setRed(false);

		setPriority(priorityOptions.info);
		setPriorityDirty(true);
	};

	const descriptionMaxLength = () => maxTotalCount - titleCount;
	const titleMaxLength = () => maxTotalCount - messageCount;
	const charCount = () => messageCount + titleCount;

	const handleDescriptionField = e => {
		const val = e.target.value;
		const valCount = val.length;

		setMessageCount(valCount);
		setMessageDescription(val);

		const isValid = validateDescription(val);

		setDescriptionValid(isValid);
		setDescriptionDirty(true);
	};

	const handleTitleField = e => {
		const val = e.target.value;
		const valCount = val.length;

		setTitleCount(valCount);
		setTitle(val);

		setTitleValid(val && val.length > 0);
		setTitleDirty(true);
	};

	const getUrl = e => {
		setConfluenceUrl(e.target.value);
	};

	const handleSwitchGlobal = ({ target }) => {
		const { checked: isGlobal } = target;

		clubs.forEach(club => {
			club.selected = isGlobal;
		});

		setClubSelectionDisabled(isGlobal);
	};

	const isMessageValid = () => {
		setDescriptionDirty(true);
		setTitleDirty(true);
		setPriorityDirty(true);

		return generateMessageToSend() && descriptionValid && titleValid && priority;
	};

	const validateDescription = (description = messageDescription) => {
		const asciiOnly = /^[\x00-\x7F]*$/;
		const excludeUrl = /(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/;

		if (
			!description ||
			!asciiOnly.test(description) ||
			excludeUrl.test(description)
		) {
			return false;
		} else {
			return true;
		}
	};

	const showDimmer = () => setDimmerActive(true);
	const hideDimmer = () => setDimmerActive(false);

	const generateMessage = async () => {
		const payload = generateMessageToSend();

		if (isMessageValid()) {
			try {
				await httpClient(authToken)
					.postAsync('messages/generate', payload)
					.then(async response => {
						const data = response.data; 
						let confirmMessage = `Recipients: ${data.recipients_count}\n\rTotal Cost: ${balance.currencySymbol}${data.total_cost}`;
						if (data.isLowBalance === true) {
							confirmMessage = `${confirmMessage}\n\rThe message can't be send due to insufficient funds.`;
							window.alert(confirmMessage);
							return;
						}
						const confirmMesssage = `Recipients: ${data.recipients_count}\n\rTotal Cost: $${data.total_cost}`;
						const confirm = window.confirm(`${confirmMesssage}\n\rDo you really want to send this message ?`); 
						
						if(confirm) { 
							return await sendMessageAsync(data, authToken);
						}
					});
			}
			catch(error) {
                console.error(error.message);
            }
        } else {
            console.log("Unable to send message");
        }
    }

    const sendMessageAsync = async (payload, authToken) => { 
		showDimmer();

		try {
			return await httpClient(authToken)
				.postAsync('messages', payload)
		}
		catch (error) {
			console.error(error.message);
		}
		finally {
			hideDimmer();
		}
	}

	const isSendButtonEnabled = clubs && clubs.length !== 0;

	const redirectPreferences = redirect => {
		if (redirect) {
			return <Redirect to="/" />;
		}
		return;
	};

	const renderClubs = () => {
		if (clubsLoading) return <div>loading....</div>;

		return (
			<ClubCheckboxContainer clubs={clubs} isDisabled={clubSelectionDisabled} onSelection={(clubs) => {
				setClubs([...clubs]);
			}} />
		);
	};
	const renderBalance = () => {
		if (balanceLoading) return <div>loading....</div>;

		return ( 
			<Balance balance={`${balance.currencySymbol}${balance.balance}`} /> 
		);
	};

	// if (clubsError) {
	// 	setRedirect(true);
	// 	redirectPreferences();
	// }

	return (
		<div>
			<Header title="Compose Messages" />
			<div className="ui padded text container segment">
				<div className="ui form">

					{/* left content */}
					<div className="left-form">
						<div className="field">
							<div className="sixteen wide field">
								<label>Title</label>
								<input
									placeholder="Input your title..."
									maxLength={titleMaxLength()}
									onChange={handleTitleField}
								/>
								<ValidationMessage
									isValid={titleValid}
									isDirty={titleDirty}
									validationText="Title is required"
								/>
							</div>
						</div>

						<div className="field">
							<label>Priority</label>
							<div className="RedInfoSwitch">
								<div className="ui buttons">
									<button className={redButton} onClick={onClickRed}>
										RED
									</button>
									<div className="or"></div>
									<button className={infoButton} onClick={onClickBlue}>
										Info
									</button>
								</div>
								<ValidationMessage
									isValid={priority}
									isDirty={priorityDirty}
									validationText="Priority is required"
								/>
							</div>
						</div>

						<div className="field">
							<label>Description</label>
							<textarea
								placeholder="Input your message..."
								maxLength={descriptionMaxLength()}
								onChange={handleDescriptionField}
							></textarea>
							<ValidationMessage
								isValid={descriptionValid}
								isDirty={descriptionDirty}
								validationText="Message body is required"
							/>
						</div>

						<div className="field">
							<label>Confluence Url</label>
							<div className="sixteen wide field">
								<input
									placeholder="Input confluence url..."
									onChange={getUrl}
								/>
							</div>
						</div>
					</div>

					{/* right content */}
					<div className="right-form">
						<div className="GlobalNameSwitch">
							<div className="ui toggle checkbox">
								<input
									type="checkbox"
									name="public"
									onChange={handleSwitchGlobal}
								/>
								<label>Global message</label>
							</div>
						</div>
						{renderClubs()}
						{renderBalance()}
					</div>

					{/* bottom content */}
					<div className="inline fields">
						<div className="six wide field left-buttons">
							<div className="field">
								{redirection(previousRedirect, '/compose/previous')}
								<Button onClick={setPreviousRedirect}>Previous Messages</Button>
							</div>
						</div>
						<div className="six wide field">
							<div className="characterCount">
								<p>
									Character count: ({charCount()}/{maxTotalCount})
								</p>
							</div>
						</div>
						<div className="six wide field right-buttons">
							{redirectPreferences(redirect)}
							<button
								className="ui button save"
								type="save"
								onClick={generateMessage}
								disabled={!isSendButtonEnabled}
							>
								Send
							</button>
							<button className="ui button" type="cancel" onClick={setRedirect}>
								Cancel
							</button>
						</div>
					</div>
				</div>
			</div>
			<Dimmer active={dimmerActive} page>
				<Loader content='Sending message' />
			</Dimmer>
		</div>
	);
};

export default withAuth(ComposeMessage);
