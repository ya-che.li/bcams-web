import React, {useEffect, useState} from "react";
import {withAuth} from "@okta/okta-react";

const Login = withAuth(({ auth }) => {
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    auth.isAuthenticated().then(result => setAuthenticated(result));
  }, [auth]);

  return (
    <h2 className="menu-option">
      {
        authenticated ? (<div onClick={e => auth.logout("/")}>Logout</div>) : (<div onClick={e => { auth.login("/"); }} > Login </div>)
      }
    </h2>
  );
});

export default Login;
