import React from "react";
import "./style.css";

const Footer = props => {
	return (
		<footer className="footer">
			<h4>
				BCAMS - Business Continuity Alert Message System &copy; 2019 AND Digital
				- All Rights Reserved
			</h4>
		</footer>
	);
};

export default Footer;
