import React, {useContext} from "react";
import {createUseStyles, useTheme} from "react-jss";
import Logo from "../../Images/logo.png";
import Logout from '../Logout';
import {AuthContext} from '../../providers/AuthProvider/AuthProvider';

const useStyles = createUseStyles({
  headerTheme: {
    backgroundColor: theme => theme.headerBackground,
    padding: "1%",
    textAlign: "right",
    color: "white",
    marginBottom: "2%",
    "& h1": {
      float: "left"
    }
  }
});

const Header = ({ title }) => {
  const theme = useTheme();
  const {auth} = useContext(AuthContext);
  const classes = useStyles(theme);

  return (
    <header className={classes.headerTheme}>
      <div className="ui small image">
        <img src={Logo} alt="AND Logo" />
          <Logout onClick={()=>{
              auth.logout("/");
          }} />
      </div>
      <h1>{title}</h1>
    </header>
  );
};

export default Header;
