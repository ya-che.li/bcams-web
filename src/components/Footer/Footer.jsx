import React from "react";
import {createUseStyles, useTheme} from "react-jss";

const useStyles = createUseStyles({
  footerTheme: {
    backgroundColor: theme => theme.footerBackground,
    position: "relative",
    height: "5rem",
    width: "100%",
    "& h4": {
      padding: "2rem 0rem 0rem 1rem",
      color: theme => theme.color,
      float: "left"
    }
  }
});

const Footer = () => {
  const theme = useTheme();
  const classes = useStyles(theme);

  return (
    <footer className={classes.footerTheme}>
      <h4>
        BCAMS - Business Continuity Alert Message System &copy; 2019 AND Digital
        - All Rights Reserved
      </h4>
    </footer>
  );
};

export default Footer;
