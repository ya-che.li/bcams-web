import React, {useEffect, useState} from 'react';
import {Button, Container, Table} from 'semantic-ui-react'
import {redirection} from './Preferences';
import httpClient from '../Helpers/http-client';
import {withAuth} from '@okta/okta-react';


import Header from './Header';

const PreviousMessageRows = ({messages}) => {
    
    if (messages) {
        return messages.map((message) => (

            <Table.Row>
                <Table.Cell collapsing>{(new Date(message.date).toDateString())}</Table.Cell>
                <Table.Cell collapsing>{message.priority}</Table.Cell>
                <Table.Cell collapsing>{message.header}</Table.Cell>
                <Table.Cell>{message.message}</Table.Cell>
                <Table.Cell collapsing>
                    {message.recipients} 
                    <a href={`/compose/previous/${message.id}`}>View</a>
                </Table.Cell>
                <Table.Cell collapsing>{message.cost}</Table.Cell>
            </Table.Row>

        ));
    } 
    return (

            <Table.Row>
                <Table.Cell collapsing>N/A</Table.Cell>
            </Table.Row>
    );
    
}

                

const PreviousMessages = ({auth}) => {

    const [redirect, setRedirect] = useState(false);
    const [authToken, setAuthToken] = useState('');  
    const [previousMessages, setPreviousMessages] = useState([]); 

    useEffect(() => {

        // Fetch list of previous messages from API
        async function fetchPreviousMessages(authToken) {
            const result = await httpClient(authToken).getAsync(`messages/`); 
            const messages = result.data;

            setPreviousMessages(messages.map((message) => (
                 {
                    id: message.id,
                    date: message.sent_date,
                    priority: message.priority_id,
                    header: message.header,
                    message: message.message,
                    recipients: message.recipients_count,
                    cost: message.cost
                 }
                
            )));
        }
        
        auth.getAccessToken().then(token=> { setAuthToken(token)});
        if(!authToken)
            return; 
        fetchPreviousMessages(authToken);
    }, [auth, authToken]);

    return (
        <div>
        <Header title="Previous Messages" />
        <Container>

            {redirection(redirect, '/compose')}
            <Button onClick={setRedirect}>Back to Compose</Button>
            
            <Table celled striped>
                <Table.Header>
                <Table.Row>
                <Table.HeaderCell>Date</Table.HeaderCell>
                    <Table.HeaderCell>Priority</Table.HeaderCell>
                    <Table.HeaderCell>Header</Table.HeaderCell>
                    <Table.HeaderCell>Message</Table.HeaderCell>
                    <Table.HeaderCell>recipients</Table.HeaderCell>
                    <Table.HeaderCell>Cost</Table.HeaderCell>
                </Table.Row>
                </Table.Header>

                <Table.Body>
                
                    <PreviousMessageRows messages={previousMessages}/>

                </Table.Body>
            </Table>
        </Container>
        </div>
    );
}

export default withAuth(PreviousMessages);
