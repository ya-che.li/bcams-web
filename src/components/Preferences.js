import React, {useEffect, useState} from 'react';
import {Redirect} from 'react-router-dom';
import Header from './Header';
import ClubCheckboxContainer from "../containers/ClubCheckboxContainer"
import ThemeForm from './ThemeForm';
import ContactDetailsForm from './ContactDetailsForm';
import {useClubFetch} from '../Hooks/Clubs';
import {useClubPreferences, usePreferences} from '../Hooks/userPreferences';
import httpClient from '../Helpers/http-client';
import CurrencyDetailsForm from './CurrencyDetailsForm';
import {withAuth} from '@okta/okta-react';

export const redirection = (redirect, destination) => { 
    if (redirect) {
        return <Redirect to={destination} />;
    }
}

const Preferences = ({ auth }) => { 
    const [authToken, setAuthToken] = useState('');   
    const [email, setEmail] = useState('');  
    const [redirect, setRedirect] = useState(false);
    const [clubs, setClubs, clubsLoading, clubsError] = useClubFetch(authToken);
    const [contact, setContact, contactMediumId, setContactMediumId, currencyId, setCurrencyId, preferencesLoading, preferencesError,theme_id, setThemeId] = usePreferences(email, authToken);
    const [userClubs, setUserClubs, isUserAssociatedToClub] = useClubPreferences(clubs, email,authToken); 
    const [isFormValid, setIsFormValid] = useState(true);
   useEffect(() => {
        auth.getUser().then(user => { 
            setEmail(user.email)
        }); 
        auth.getAccessToken().then(token=> { setAuthToken(token)}); 

    }, [auth]);
    // if (clubsError || preferencesError) {
    //     return <Redirect to="/error" />;
    // } 
    const displayNoClubAssociation= ()=> {
        if(!isUserAssociatedToClub){
            return (<p className="alert alert-warning">You are not subscribed to any club, please subscribe to a club.</p>);
        }
        return (<div />);
    } 

    const handleSave = async (e) => {

        e.preventDefault();

        const selectedClubIds = clubs
            .filter((club) => club.selected)
            .map((selectedClub) => selectedClub.id);

        const payload = {
            email: email,
            contact_medium_id: contactMediumId,
            contact: contact,
            affected_club_ids: selectedClubIds, 
            currency_id: currencyId,
            theme_id: theme_id
        }

        await httpClient(authToken).postAsync('preferences', payload); 
    };

    const isContactFormValid = (isValid) => {
        setIsFormValid(isValid);
    }

    return (
        <div>
            <Header title="Preferences" />
            <div className="ui padded text container segment">
            {displayNoClubAssociation()} 
                <form className="ui form"> 
                    <div className="left-form"> 
                        <ThemeForm value={theme_id} authToken ={authToken} onSelection={theme_id => {
                            setThemeId(theme_id);
                        }} />
                        <ContactDetailsForm
                            contact={contact} 
                            setContact={setContact}
                            contactMediumId={contactMediumId}
                            contactMediumIdChanged={setContactMediumId}
                            isContactFormValid={isContactFormValid} 
                            authToken={authToken}/>

                        <CurrencyDetailsForm
                            currencyId={currencyId}
                            currencyIdChanged={setCurrencyId}
                            authToken={authToken} /> 
                    </div>
                    <div className="right-form">
                        <ClubCheckboxContainer clubs={userClubs} onSelection={(clubs) => {
                            setUserClubs([...clubs]);
                        }} />
                    </div>

                    <div className="inline fields">
                        <div className="ten wide field left-buttons">
                            {redirection(redirect, '/compose')}
                            <button className="ui button" onClick={setRedirect}>Compose Alert</button>
                            <button className="ui button" type="admin">User Admin</button>
                        </div>

                        <div className="six wide field right-buttons">
                            <button className="ui button save" type="save" onClick={handleSave} disabled={!isFormValid}>
                                Save
                            </button>
                            <button className="ui button" type="cancel">
                                Discard
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default withAuth(Preferences);
