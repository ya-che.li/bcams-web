import React, {useState} from "react";
import TextInput from '../shared/TextInput';
import LabelField from '../LabelField';
import PriorityInput from '../PriorityInput';
import TextArea from '../shared/TextArea';
import GlobalMessage from '../GlobalMessage';
import Balance from '../Balance';
import TextButton from '../shared/TextButton';
import CharacterCount from '../CharacterCount';
import {COLOR_INFO, COLOR_RED} from '../PriorityInput/PriorityInput';
import ClubCheckboxList from '../ClubCheckboxList';
import {MAX_TOTAL_MESSAGE_COUNT} from '../../config';
import Loading from '../shared/Loading';
import ValidationMessage from '../shared/ValidationMessage';
import FullScreenLoading from '../shared/FullScreenLoading';

function isStringEmpty(str=""){
    return str.trim().length===0;
}

function getIsSendButtonDisabled({is_clubs_loading, is_balance_loading, isTitleDirty, isTitleValid, isMessageDirty, isMessageValid, isPriorityIdDirty, isPriorityValid, isAffectedClubIdDirty, isAffectedClubIdValid}){
    return is_clubs_loading || is_balance_loading || (isTitleDirty && !isTitleValid) || (isMessageDirty && !isMessageValid)  || (isPriorityIdDirty && !isPriorityValid) || (isAffectedClubIdDirty && !isAffectedClubIdValid);
}

const ComposeMessage = ({header, setHeader, priority_id, setPriorityId, description, setDescription, url, setUrl, clubs, is_clubs_loading, affected_club_ids, setAffectedClubIds, is_global, setIsGlobal, currencySymbol, is_balance_loading, balance, onPreviousMessageClick, onCancelClick, onSendClick, is_saving=false}) => {
    const [isHeaderDirty, setIsHeaderDirty] = useState(false);
    const [isPriorityIdDirty, setIsPriorityIdDirty] = useState(false);
    const [isDescriptionDirty, setIsDescriptionDirty] = useState(false);
    const [isAffectedClubIdDirty, setIsAffectedClubIdsDirty] = useState(false);

    const maxHeaderCount = MAX_TOTAL_MESSAGE_COUNT;
    const headerCount = header.length;
    const descriptionCount = description.length;
    const headerMaxLength = maxHeaderCount - descriptionCount;
    const messageMaxLength = maxHeaderCount - headerCount;
    const characterCount = descriptionCount + headerCount;

    const isHeaderValid = !isStringEmpty(header);
    const isDescriptionValid = !isStringEmpty(description);
    const isPriorityValid = !!priority_id;
    const isAffectedClubIdValid = affected_club_ids.length!==0;

    const isSendButtonDisabled = getIsSendButtonDisabled({is_clubs_loading, is_balance_loading, isTitleDirty: isHeaderDirty, isTitleValid: isHeaderValid, isDescriptionDirty, isDescriptionValid, isPriorityIdDirty, isPriorityValid, isAffectedClubIdDirty, isAffectedClubIdValid});

    return (
        <div className="ui padded text container segment">
            <div className="ui form">
                <div className="left-form">
                    <div className="field">
                        <LabelField label="Title">
                            <TextInput placeholder="Input your title..." maxLength={headerMaxLength} value={header} onChange={(title)=>{
                                setHeader(title);
                                setIsHeaderDirty(true);
                            }} />
                            <ValidationMessage
                                isDirty={isHeaderDirty}
                                isValid={isHeaderValid}
                                validationText="Title is required"
                            />
                        </LabelField>
                        <LabelField label="Priority">
                            <PriorityInput value={priority_id} onBlueClick={()=>{
                                setPriorityId(COLOR_INFO);
                            }} onRedClick={()=>{
                                setPriorityId(COLOR_RED);
                            }} />
                            <ValidationMessage
                                isDirty={isPriorityIdDirty}
                                isValid={isPriorityValid}
                                validationText="Priority is required"
                            />
                        </LabelField>
                        <LabelField label="Description">
                            <TextArea maxLength={messageMaxLength} placeholder="Input your message..." value={description} onChange={description=>{
                                setDescription(description);
                                setIsDescriptionDirty(true);
                            }} />
                            <ValidationMessage
                                isDirty={isDescriptionDirty}
                                isValid={isDescriptionValid}
                                validationText="Message body is required"
                            />
                        </LabelField>
                        <LabelField label="Confluence Url">
                            <TextInput maxLength={255} placeholder="Input confluence url..." value={url} onChange={setUrl} />
                        </LabelField>
                    </div>
                </div>
                <div className="right-form">
                    <GlobalMessage checked={is_global} onChange={checked=>{
                        if(checked){
                            setAffectedClubIds([...clubs].map(e => e.id));
                        }
                        else{
                            setAffectedClubIds([]);
                        }
                        setIsGlobal(checked);
                        setIsAffectedClubIdsDirty(true);
                    }} />
                    <ClubCheckboxList is_disabled_all={is_global} clubs={clubs} loading={is_clubs_loading} values={affected_club_ids} onChange={({id, checked})=>{
                        if(checked){
                            setAffectedClubIds([...affected_club_ids, id]);
                        }
                        else{
                            const _affected_clubs_ids = [...affected_club_ids];
                            const index = _affected_clubs_ids.indexOf(id);
                            if (index > -1) {
                                _affected_clubs_ids.splice(index, 1);
                                setAffectedClubIds(_affected_clubs_ids);
                            }
                        }
                        setIsAffectedClubIdsDirty(true);
                    }} />

                    <ValidationMessage
                        isDirty={isAffectedClubIdDirty}
                        isValid={isAffectedClubIdValid}
                        validationText="Affected clubs is required"
                    />
                    {is_balance_loading ? <Loading />: <Balance currencySymbol={currencySymbol} balance={balance} />}
                </div>
                <div className="inline fields">
                    <div className="six wide field left-buttons">
                        <TextButton text="Previous Messages" onClick={onPreviousMessageClick} />
                    </div>
                    <div className="six wide field">
                        <CharacterCount count={characterCount} maxTotalCount={maxHeaderCount} />
                    </div>
                    <div className="six wide field right-buttons">
                        <TextButton disabled={isSendButtonDisabled} text="Send" onClick={()=>{
                            setIsHeaderDirty(true);
                            setIsPriorityIdDirty(true);
                            setIsDescriptionDirty(true);
                            setIsAffectedClubIdsDirty(true);

                            if(!getIsSendButtonDisabled({is_clubs_loading, is_balance_loading, isTitleDirty: true, isTitleValid: isHeaderValid, isMessageDirty: true, isMessageValid: isDescriptionValid, isPriorityIdDirty: true, isPriorityValid})){
                                onSendClick();
                            }
                        }} />
                        <TextButton text="Cancel" onClick={onCancelClick} />
                    </div>
                </div>
            </div>
            {is_saving && <FullScreenLoading content='Sending message' />}
        </div>
    );
};

export default ComposeMessage;
