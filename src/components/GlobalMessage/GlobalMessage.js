import React from 'react';
import Checkbox from '../shared/Checkbox';

export default function GlobalMessage({checked, onChange}){
    return <div className="ui toggle checkbox">
        <Checkbox checked={checked} onChange={onChange} />
        <label>Global message</label>
    </div>
}
