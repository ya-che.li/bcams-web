import React, {useEffect, useState} from 'react';
import {Dropdown} from 'semantic-ui-react';
import httpClient from '../Helpers/http-client';

export const contactMediums = {};

const useContactOptions = (authToken) => {
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const fetchContactOptions = async (authToken) => {
      try {
        const jsonResponse = await httpClient(authToken).getAsync('contact-medium');

        setOptions(jsonResponse.data.map(opt => ({
          key: opt.id,
          value: opt.id,
          text: opt.name,
        })));

        jsonResponse.data.forEach(opt => {
          contactMediums[opt.name.toLowerCase()] = opt.id;
        });
      } finally {
        setLoading(false);
      }
    };
    if(!authToken)
      return; 
    fetchContactOptions(authToken);
  }, [authToken]);

  return [options, loading];
};

const ContactDropdown = ({ contactMediumId, onChange,authToken }) => {
  const [options, loading] = useContactOptions(authToken);

  return (<Dropdown
    placeholder='Contact'
    selection
    value={contactMediumId} 
    options={options} 
    onChange={onChange}
    loading={loading}
  />);
}

export default ContactDropdown;
