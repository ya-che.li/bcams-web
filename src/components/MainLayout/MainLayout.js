import React, {useContext} from "react";
import "../style.css";
import {createUseStyles, ThemeProvider} from "react-jss";
import Footer from "../Footer";
import Header from "../Header";
import routes from "../../routes/index";
import {PreferencesContext} from '../../providers/PreferencesProvider/PreferencesProvider';

const useStyles = createUseStyles({
    main: {
        display: `flex`,
        flex: 1,
        flexDirection: `column`
    },
    root: {
        display: "flex",
        flexDirection: "column",
        minHeight: "100vh"
    }
});

function getHeaderTitle(path) {
    return routes.find(e => e.path===path).headerTitle || "";
}

const MainLayout = props => {
    const [{theme_id=""}] = useContext(PreferencesContext);
    const classes = useStyles();
    const headerTitle = getHeaderTitle(props.match.path);

    return (
        <ThemeProvider theme={themeChooser(theme_id)}>
            <div className={classes.root}>
                <Header title={headerTitle}/>
                <main className={classes.main}>
                    <props.component {...props} />
                </main>
                <Footer/>
            </div>
        </ThemeProvider>
    );
};

export default MainLayout;
