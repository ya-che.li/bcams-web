import React from 'react';
import ContactDropdown, {contactMediums} from './ContactDropdown';
import {get_email_disabled_text, get_mobile_disabled_text} from '../Helpers/Validators';

const ContactDetailsForm = ({ contact, email, setContact, contactMediumId, contactMediumIdChanged, isContactFormValid,authToken }) => {  

	const getErrorText = () => {
		let errorText = get_mobile_disabled_text(contact);

		if (contactMediumId === contactMediums.email) {
			errorText = get_email_disabled_text(contact);
		}

		return errorText;
	}

	let errorText = getErrorText(contactMediumId);

	const onContactMediumChange = (event, data) => {  

		contactMediumIdChanged(data.value);
		setDefaultEmail(data.value);
	}

	const runValidation = () => {
		errorText = getErrorText(contactMediumId);

		if (errorText) {
			isContactFormValid(false);

			return (
				<p style={{ color: "red" }}>
					{errorText}
				</p>
			);
		} else {
			isContactFormValid(true);
		}
	}
	const displayMobileInput = () => {
		return (
			<input
				type="text"
				placeholder="contact Number"
				value={contact}
				onChange={e => {setContact(e.target.value);
				}}
			/>
		);
	}
	const setDefaultEmail = (value)=>{   
		if(value === contactMediums.email && isNaN(contact) === false){ 
			setContact(email);
		} 
	}
	const displayEmailInput = () => {
		return (
			<input
				type="email"
				placeholder="contact Email"
				value={contact}
				onChange={e => {
					setContact(e.target.value);
				}}
			/>
		);
	}
	function displayInput() { 
		switch (contactMediumId) {
			case contactMediums.email:{ 
				return (displayEmailInput());}
			case contactMediums.sms:
				return (displayMobileInput());
			default: return '';
		}
	}
	return (
		<div className="field">
			<label>Contact Details</label>

			<div className="four wide field">
				<ContactDropdown contactMediumId={contactMediumId} onChange={onContactMediumChange} authToken={authToken} />
			</div>

			<div className="twelve wide field">
				{displayInput()}
				{runValidation()}
			</div>
		</div>
	);
};

export default ContactDetailsForm;
