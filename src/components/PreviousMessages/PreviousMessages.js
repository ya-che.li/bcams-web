import React from 'react';
import {Container, Table} from 'semantic-ui-react'
import TextButton from '../shared/TextButton';

const PreviousMessageRows = ({messages}) => {
    if (messages) {
        return messages.map((message, index) => (
            <Table.Row key={`PreviousMessageRows_${index}`}>
                <Table.Cell collapsing>{(new Date(message.date).toDateString())}</Table.Cell>
                <Table.Cell collapsing>{message.priority}</Table.Cell>
                <Table.Cell collapsing>{message.header}</Table.Cell>
                <Table.Cell>{message.message}</Table.Cell>
                <Table.Cell collapsing>
                    {message.recipients}
                    <a href={`/compose/previous/${message.id}`}>View</a>
                </Table.Cell>
                <Table.Cell collapsing>{message.cost}</Table.Cell>
            </Table.Row>

        ));
    }
    return <Table.Row>
        <Table.Cell collapsing>N/A</Table.Cell>
    </Table.Row>
};

const PreviousMessages = ({onBackClick, previousMessages=[]}) => {
    return (
        <div>
            <Container>
                <TextButton text="Back to Compose" onClick={onBackClick} />
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Date</Table.HeaderCell>
                            <Table.HeaderCell>Priority</Table.HeaderCell>
                            <Table.HeaderCell>Header</Table.HeaderCell>
                            <Table.HeaderCell>Message</Table.HeaderCell>
                            <Table.HeaderCell>recipients</Table.HeaderCell>
                            <Table.HeaderCell>Cost</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>

                        <PreviousMessageRows messages={previousMessages}/>

                    </Table.Body>
                </Table>
            </Container>
        </div>
    );
};

export default PreviousMessages;
