import React from 'react';
import {Button} from 'semantic-ui-react'

export const COLOR_RED = 13;
export const COLOR_INFO = 14;

export default function PriorityInput({value, onRedClick, onBlueClick}){
    const red_color = value === COLOR_RED ? "red" : undefined;
    const info_color = value === COLOR_INFO ? "blue" : undefined;

    return <Button.Group>
        <Button onClick={onRedClick} color={red_color}>RED</Button>
        <Button.Or />
        <Button onClick={onBlueClick} color={info_color}>Info</Button>
    </Button.Group>
}
