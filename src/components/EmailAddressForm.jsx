import React from 'react';

const EmailAddressForm = () => {
    return (
        <div className="field">         
            <div className="sixteen wide field">
                <label>Email Address</label>
                <p>stephane.lobognon@and.digital</p>
            </div>
        </div>
    );
}

export default EmailAddressForm;