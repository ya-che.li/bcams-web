import React from 'react';

const Logout = (({onClick}) => {
    return (
        <h2 className="menu-option">
            {
                <span onClick={onClick}>Logout</span>
            }
        </h2>
    );
});

export default Logout;
