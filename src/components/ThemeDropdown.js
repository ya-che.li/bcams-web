import React, {useEffect, useState} from 'react';
import {Dropdown} from 'semantic-ui-react';
import httpClient from '../Helpers/http-client';

export const themeOptions = {};

const useThemeOptions = (authToken) => {
    const [options, setOptions] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(authToken === '' || authToken === undefined)
            return; 
        setLoading(true);
        const fetchThemeOptions = async (authToken) => {
            try {
                const jsonResponse = await httpClient(authToken).getAsync('themes');

                setOptions(jsonResponse.data.map((opt, index) => ({
                    key: `${index}`,
                    value: opt.id,
                    text: opt.colour
                })));
             
                jsonResponse.data.forEach(opt => {
                    themeOptions[opt.colour] = opt.id;
                });
            } finally {
                setLoading(false);
            }
        };
        if(!authToken)
            return; 
        fetchThemeOptions(authToken);
    }, [authToken]);

    return [options, loading];
};

const ThemeDropdown = ({ themeOption, onChange, authToken}) => {
    const [options, loading] = useThemeOptions(authToken); 
    return (<Dropdown
        placeholder='Theme'
        selection
        value={themeOption}
        options={options}
        onChange={onChange}
        loading={loading}
    />);
}

export default ThemeDropdown;
