import React from 'react';
import {createUseStyles} from 'react-jss';
import {useParams} from 'react-router-dom';

const useStyles = createUseStyles({
    root: {
        margin: "5em",
        fontSize: "1.2rem",
        textAlign: "center"
    }
});

const ErrorPage = ({ history }) => {
    const classes = useStyles();
    const {status, message} = useParams();

    return <div className={classes.root}>
        <h1>Oops! Something went wrong.</h1>
        <h1>{status}</h1>
        <p>Please contact AND administrator or go back to <button  className="ui button" onClick={history.goBack}>previous page</button>.</p>
        <details style={{ whiteSpace: 'pre-wrap'  }}>
            <div>Error: {message}</div>
        </details>
    </div>
};

export default ErrorPage;
