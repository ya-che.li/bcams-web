import React from 'react';

const Balance = ({ balance }) => {
    return (
        <div className="ui balance">
            <b>Remaining Balance: </b>
            <label>{balance}</label>
        </div>
    );
}

export default Balance;