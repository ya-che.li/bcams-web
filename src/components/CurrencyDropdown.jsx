import React, {useEffect, useState} from 'react';
import {Dropdown} from 'semantic-ui-react';
import httpClient from '../Helpers/http-client';

export const currencies = {};

const useCurrencyOptions = (authToken) => {
  const [options, setOptions] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const fetchCurrencyOptions = async (authToken) => {
      try {
        const jsonResponse = await httpClient(authToken).getAsync('currencies');

        setOptions(jsonResponse.data.map(opt => ({
          key: opt.id,
          value: opt.id,
          text: opt.currency,
        })));

        jsonResponse.data.forEach(opt => {
          currencies[opt.currency.toLowerCase()] = opt.id;
        });
      } finally {
        setLoading(false);
      }
    };

    if(!authToken)
      return;
    fetchCurrencyOptions(authToken);
  }, [authToken]);

  return [options, loading,authToken];
};

const CurrencyDropdown = ({ currencyId, onChange, authToken }) => {
  const [options, loading] = useCurrencyOptions(authToken);

  return (<Dropdown
    placeholder='Currency'
    selection
    value={currencyId}
    options={options}
    onChange={onChange}
    loading={loading}
  />);
}

export default CurrencyDropdown;
