import React from "react";
import css from "./ValidationMessage.module.css";

const ValidationMessage = ({ isValid, isDirty, validationText }) => {
	if (!isDirty || isValid) return "";

	return (
		<div className={css.error}>
			<span>{validationText}</span>
		</div>
	);
};

export default ValidationMessage;
