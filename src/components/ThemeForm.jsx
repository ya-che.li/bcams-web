import React from 'react';
import ThemeDropdown from './ThemeDropdown';

const ThemeForm = ({ value, onSelection ,authToken}) => {
    return (
        <div className="field">
            <label>Theme</label>
            <div className="four wide field"> 
                <ThemeDropdown themeOption={value} authToken ={authToken} onChange={(event, {value})=>{
                    onSelection(value);
                }} />
            </div>
        </div>
    );
}

export default ThemeForm;