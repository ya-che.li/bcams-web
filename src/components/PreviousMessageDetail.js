import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import Header from './Header';
import {Button, Container, Table} from 'semantic-ui-react';
import {redirection} from './Preferences';
import httpClient from '../Helpers/http-client';
import {withAuth} from '@okta/okta-react';


const PreviousMessageDetailRows = ({messages}) => {
    if (messages) {

        return messages.map((message) => (
            <Table.Row>
                <Table.Cell collapsing>{(new Date(message.date).toDateString())}</Table.Cell>
                <Table.Cell collapsing>{message.email}</Table.Cell>
                <Table.Cell collapsing>{message.firstname}</Table.Cell>
                <Table.Cell collapsing>{message.lastname}</Table.Cell>
                <Table.Cell>{message.medium}</Table.Cell>
            </Table.Row>
        ));
    } 
}

const PreviousMessageDetail = ({auth}) => {
    const [redirect, setRedirect] = useState(false);
    const [authToken, setAuthToken] = useState('');  
    const[previousMessageDetail, setPreviousMessageDetail] = useState([]);
    const params = useParams(); 

    useEffect(() => {

        // Get detailed data from API on a message
        async function fetchPreviousMessageDetail(authToken) {
            const result = await httpClient(authToken).getAsync(`messages/${params.id}/sent`); 
            const recipients = result.data;

            setPreviousMessageDetail(recipients.map((recipient) => (
                 {
                    email: recipient.receiver_id,
                    firstname: recipient.receiver_firstname,
                    lastname: recipient.receiver_lastname,
                    medium: recipient.contact_medium,
                    date: recipient.sent_date
                 } 
            )));
        }
        auth.getAccessToken().then(token=> { setAuthToken(token)});
        if(!authToken)
            return; 
        fetchPreviousMessageDetail(authToken);
    }, [auth, authToken]);


    return (
        <div>
            <Header title="Previous Message Detail" />
            <Container>
                {redirection(redirect, '/compose/previous/')}
                <Button onClick={setRedirect}>Back to Previous Message List</Button>
                
                <Table celled striped>
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Date</Table.HeaderCell>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>First Name</Table.HeaderCell>
                        <Table.HeaderCell>Last Name</Table.HeaderCell>
                        <Table.HeaderCell>Medium</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        <PreviousMessageDetailRows messages={previousMessageDetail}/>
                    </Table.Body>
                </Table>
            </Container>
        </div>
    );
}

export default withAuth(PreviousMessageDetail);
