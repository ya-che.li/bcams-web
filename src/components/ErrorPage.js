import React from 'react';
import {useParams} from 'react-router-dom';
import Header from './Header';
import './style.css';

const ErrorPage = ({ history }) => {
    const params = useParams();  
    return (<div>
        <Header title="Error" />
        <div className="error-section">
            <h1>Oops! Something went wrong.</h1> 
            <h1>{params.status}</h1>
            <p>Please contact AND administrator or go back to <button  className="ui button" onClick={history.goBack}>previous page</button>.</p>
            <details style={{ whiteSpace: 'pre-wrap'  }}>
                <div>Error: {params.message}</div>
            </details>
        </div>
    </div>);
}

export default ErrorPage;
