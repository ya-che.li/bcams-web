import React from 'react';
import './style.css'
import Logo from '../Images/logo.png'
import Login from './Login/login';


const Header = (props) => {
    return (
        <div className="header">
            <div className="ui small image">
                <img src={Logo} alt="AND Logo"></img>
                <Login/>
            </div>
            <h1>{props.title}</h1>
        </div>
    );
}

export default Header;