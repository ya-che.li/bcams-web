import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import ValidationMessage from './ValidationMessage';

export default {
    title: "ValidationMessage",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const isValid = boolean("isValid", false);
    const isDirty = boolean("isDirty", false);
    const validationText = text("validationText", "validationText");

    return <ValidationMessage
        isValid={isValid}
        isDirty={isDirty}
        validationText={validationText}
    />;
};

export const NotValidAndDirty = () => {
    const isValid = boolean("isValid", false);
    const isDirty = boolean("isDirty", true);
    const validationText = text("validationText", "validationText");

    return <ValidationMessage
        isValid={isValid}
        isDirty={isDirty}
        validationText={validationText}
    />;
};
