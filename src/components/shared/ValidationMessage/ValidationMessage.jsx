import React from "react";
import PropTypes from "prop-types";
import css from "./ValidationMessage.module.css";

const ValidationMessage = ({ isValid, isDirty, validationText }) => {
	if (!isDirty || isValid) {
        return "";
    }

	return (
		<div className={css.error}>
			<span>{validationText}</span>
		</div>
	);
};

export default ValidationMessage;

ValidationMessage.defaultProps = {
    isDirty: false,
    validationText: ""
};

ValidationMessage.propTypes = {
    isValid: PropTypes.bool.isRequired,
    isDirty: PropTypes.bool,
    validationText: PropTypes.string
};
