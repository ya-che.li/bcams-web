import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import TextButton from './TextButton';

export default {
    title: "TextButton",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const disabled = boolean("disabled", false);
    const buttonText = text("text", "Text");

    return <TextButton
        test_id="test_id"
        text={buttonText}
        onClick={action('onClick')}
        disabled={disabled}
    />
};
