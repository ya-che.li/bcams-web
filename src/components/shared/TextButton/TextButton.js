import PropTypes from "prop-types";

export default function TextButton({test_id="", text, onClick, disabled=false}){
    return <button test_id={test_id} disabled={disabled} className="ui button" onClick={e=>{
        e.preventDefault();
        onClick();
    }}>
        {text}
    </button>
}

TextButton.defaultProps = {
    test_id: "",
    text: "",
    onClick: ()=>{},
    disabled: false
};

TextButton.propTypes = {
    test_id: PropTypes.string,
    text: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool
};
