import {withKnobs} from "@storybook/addon-knobs";
import Loading from './Loading';

export default {
    title: "Loading",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    return <Loading />
};
