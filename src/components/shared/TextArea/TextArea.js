import PropTypes from 'prop-types';

export default function TextArea({placeholder, maxLength, value, onChange}){
    return (
        <textarea
            placeholder={placeholder}
            maxLength={maxLength}
            value={value}
            onChange={(e)=>{
                onChange(e.target.value);
            }}
        >
        </textarea>
    );
};

TextArea.defaultProps = {
    placeholder: "",
    value: "",
    onChange: ()=>{}
};

TextArea.propTypes = {
    placeholder: PropTypes.string,
    maxLength: PropTypes.number.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func
};
