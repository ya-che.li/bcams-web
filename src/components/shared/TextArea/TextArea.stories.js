import {number, text, withKnobs} from "@storybook/addon-knobs";
import TextArea from './TextArea';

export default {
    title: "TextArea",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const placeholder = text("placeholder", "");
    const maxLength = number("maxLength", 10);
    const value = text("value", "Value");

    return <TextArea
        placeholder={placeholder}
        maxLength={maxLength}
        value={value}
        onChange={action('onChange')}
    />
};
