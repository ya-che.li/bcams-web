import {object, select, text, withKnobs} from "@storybook/addon-knobs";
import DropdownList from './DropdownList';

export default {
    title: "DropdownList",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const value = select("value", ["value1", "value2"], "value1");
    const placeholder = text("placeholder", "placeholder");
    const options = object("options", [{value: "value1", text: "text1"}, {value: "value2", text: "text2"}]);

    return <DropdownList
        value={value}
        placeholder={placeholder}
        options={options}
        onChange={action('onChange')}
    />
};
