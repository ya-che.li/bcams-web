import React from 'react';
import PropTypes from 'prop-types';
import {Dropdown} from 'semantic-ui-react';

const DropdownList = ({ value, onChange=()=>{}, options=[], placeholder="", test_id="" }) => {
    return <div className="field">
        <div className="four wide field">
            <Dropdown
                test_id={test_id}
                placeholder={placeholder}
                selection
                value={value}
                options={options}
                onChange={(e, {value})=>{
                    onChange(value);
                }}
            />
        </div>
    </div>
};

DropdownList.defaultProps = {
    value: "",
    options: [],
    onChange: ()=>{},
    placeholder: "",
    test_id: ""
};

DropdownList.propTypes = {
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    options: PropTypes.array,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
    test_id: PropTypes.string
};

export default DropdownList;
