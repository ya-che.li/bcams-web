import {number, text, withKnobs} from "@storybook/addon-knobs";
import EmailInput from './EmailInput';

export default {
    title: "EmailInput",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const placeholder = text("placeholder", "");
    const maxLength = number("maxLength", 255);
    const value = text("value", "go@and.digital");

    return <EmailInput
        placeholder={placeholder}
        value={value}
        maxLength={maxLength}
        onChange={action('onChange')}
    />
};
