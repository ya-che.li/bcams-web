import PropTypes from 'prop-types';
import Input from '../Input';

export default function EmailInput({maxLength, placeholder, value, onChange, ...props}){
    return (
        <Input {...props}
            type="email"
            maxLength={maxLength}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
        />
    );
};

EmailInput.defaultProps = {
    placeholder: "",
    value: "",
    onChange: ()=>{}
};

EmailInput.propTypes = {
    maxLength: PropTypes.number.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};
