import PropTypes from 'prop-types';
import {Dimmer, Loader} from 'semantic-ui-react';

export default function FullScreenLoading({content}){
    return <Dimmer active page>
        <Loader content={content} />
    </Dimmer>
}

FullScreenLoading.defaultProps = {
    content: ""
};

FullScreenLoading.propTypes = {
    content: PropTypes.string
};
