import {number, select, text, withKnobs} from "@storybook/addon-knobs";
import Input from './Input';

export default {
    title: "Input",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const type = select("type", ["text", "email"], "text");
    const placeholder = text("placeholder", "placeholder");
    const value = text("value", "value");
    const maxLength = number("maxLength", 10);

    return <Input
        type={type}
        maxLength={maxLength}
        placeholder={placeholder}
        value={value}
        onChange={action('onChange')}
    />
};
