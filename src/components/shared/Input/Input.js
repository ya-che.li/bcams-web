import PropTypes from 'prop-types';

export default function Input({type, maxLength, placeholder, value, onChange}) {
    return (
        <input
            type={type}
            placeholder={placeholder}
            maxLength={maxLength}
            value={value}
            onChange={({target: {value}}) => {
                onChange(value);
            }}
        />
    );
};

Input.defaultProps = {
    placeholder: "",
    value: "",
    onChange: () => {}
};

Input.propTypes = {
    type: PropTypes.oneOf(["text", "email"]),
    maxLength: PropTypes.number.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};
