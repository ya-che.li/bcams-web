import {boolean, withKnobs} from "@storybook/addon-knobs";
import Checkbox from './Checkbox';

export default {
    title: "Checkbox",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const checked = boolean("checked", false);

    return <Checkbox
        checked={checked}
        onChange={action("onChange")}
    />
};
