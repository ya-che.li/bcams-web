import React from 'react';
import PropTypes from 'prop-types';

export default function Checkbox({checked, onChange, ...props}){
    return <input type="checkbox" {...props} checked={checked} onChange={({target:{checked}})=>{
        onChange(checked);
    }} />;
};

Checkbox.defaultProps = {
    checked: false,
    onChange: ()=>{}
};

Checkbox.propTypes = {
    checked: PropTypes.bool,
    onChange: PropTypes.func
};
