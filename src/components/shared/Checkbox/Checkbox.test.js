import Checkbox from '../Checkbox';
import renderer from 'react-test-renderer';

test('<Checkbox />', () => {
    const component = renderer.create(
        <Checkbox />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
