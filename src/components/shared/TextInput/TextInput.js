import PropTypes from 'prop-types';
import Input from '../Input';

export default function TextInput({maxLength, placeholder, value, onChange, ...props}){
    return (
        <Input
            {...props}
            type="text"
            maxLength={maxLength}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
        />
    );
};

TextInput.defaultProps = {
    placeholder: "",
    value: "",
    onChange: ()=>{}
};

TextInput.propTypes = {
    maxLength: PropTypes.number.isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func
};
