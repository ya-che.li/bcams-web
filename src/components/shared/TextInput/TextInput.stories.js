import {number, text, withKnobs} from "@storybook/addon-knobs";
import TextInput from './TextInput';

export default {
    title: "TextInput",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const Default = () => {
    const maxLength = number("maxLength", 10);
    const placeholder = text("placeholder", "placeholder");
    const value = text("value", "value");

    return <TextInput
        maxLength={maxLength}
        placeholder={placeholder}
        value={value}
        onChange={action('onChange')}
        onClick={action('onClick')}
    />
};
