import React from 'react';
import Footer from './components/Footer'
import Preferences from './components/Preferences';
import ComposeMessage from './components/ComposeMessage';
import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import ErrorPage from './components/ErrorPage';
import PreviousMessages from './components/PreviousMessages';
import PreviousMessageDetail from './components/PreviousMessageDetail';
import {SecureRoute} from "@okta/okta-react";
import {ImplicitCallback, Security} from "./Security/security";


const App = () => {
    return (
        <div> 
                
                <Router>
                <Security> 
                    <Switch>
                        <Route path="/implicit/callback" component={ImplicitCallback} />
                        <SecureRoute exact path="/" component={Preferences} />
                        <SecureRoute  exact path="/compose" component={ComposeMessage} />
                        <SecureRoute  exact path="/compose/previous" component={PreviousMessages} />
                        <SecureRoute  exact path="/compose/previous/:id" component={PreviousMessageDetail} />  
                        <Route path="/error/:status/:message?" component={ErrorPage} /> 
                        <Route path ="*">
                            <Redirect to={"/error/404/Page Not Found"} />
                        </Route>
                    </Switch>
                </Security>
                </Router> 
            <Footer />
        </div>
    );
}

export default App;
