import {TEST_TOKEN} from './config';

export default class utils {
    static redirect(history, url="") {
        history.push(url);
    }

    static refreshPage() {
        window.location.reload();
    }

    static getStorageItem(key=""){
        return window.localStorage.getItem(key);
    }

    static isTestingMode(){
        return this.getStorageItem("test") === "true";
    }

    static getAccessToken(){
        if(this.isTestingMode()){
            return TEST_TOKEN;
        }
        else{
            const {accessToken:{accessToken}} = JSON.parse(utils.getStorageItem("okta-token-storage"));
            return accessToken;
        }
    }
}
