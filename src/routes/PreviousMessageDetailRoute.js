import PreviousMessageDetail from '../components/PreviousMessageDetail';
import utils from '../utils';
import usePreviousMessageDetail from '../Hooks/usePreviousMessageDetail';
import Loading from '../components/shared/Loading';

function map({receiver_id, receiver_firstname, receiver_lastname, contact_medium, sent_date}){
    return {
        email: receiver_id,
        firstname: receiver_firstname,
        lastname: receiver_lastname,
        medium: contact_medium,
        date: sent_date
    };
}

const PreviousMessageDetailRoute = ({history, match:{params: {id}}}) => {
    const [messages, loading] = usePreviousMessageDetail(id);

    if(loading){
        return <Loading />
    }
    else{
        return <PreviousMessageDetail messages={messages.map(map)} onBackClick={()=>{
            utils.redirect(history, "/compose/previous");
        }} onResendMessageClick={()=>{
            utils.redirect(history, `/compose/${id}`);
        }} />
    }
};

export default PreviousMessageDetailRoute;
