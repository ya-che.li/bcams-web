import {useContext, useState} from 'react';
import Preferences from '../components/Preferences';
import useUserClubs from '../Hooks/useUserClubs';
import httpClient from '../Helpers/http-client';
import utils from '../utils';

import FullScreenLoading from '../components/shared/FullScreenLoading';
import useUser from '../Hooks/useUser';
import {ROLE_ADMIN, ROLE_COMPOSER} from '../constants/roles';
import {PreferencesContext} from '../providers/PreferencesProvider/PreferencesProvider';

function onSaveClick({email, contact_medium_id, contact, userClubs, currency_id, theme_id, setIsSaving}){
    setIsSaving(true);

    const payload = {
        email,
        contact_medium_id,
        contact,
        affected_club_ids: userClubs.map(e=>e.club_id),
        currency_id,
        theme_id
    };
    return httpClient().postAsync('preferences', payload).finally(()=>{
        setIsSaving(false);
    });
}

const PreferencesRoute = ({history}) => {
    const [isSaving, setIsSaving] = useState(false);
    const [preference, setPreference, loading, error] = useContext(PreferencesContext);

    const {email, theme_id, contact, contact_medium_id, currency_id} = preference;
    const [userClubs=[], setUserClubs] = useUserClubs(email);
    const [{roles=[]}] = useUser(email);

    const role_names = roles.map(e => e.name);
    const isAdmin = role_names.indexOf(ROLE_ADMIN)!==-1;
    const isComposer = role_names.indexOf(ROLE_COMPOSER)!==-1;

    if(isSaving){
        return <FullScreenLoading />
    }
    else{
        return (
            <Preferences
                email={email}
                theme_id={theme_id}
                setThemeId={(theme_id)=>{
                    setPreference({...preference, theme_id});
                }}
                contact={contact}
                setContact={(contact)=>{
                    setPreference({...preference, contact});
                }}
                contact_medium_id={contact_medium_id}
                setContactMediumId={(contact_medium_id)=>{
                    setPreference({...preference, contact_medium_id});
                }}
                currency_id={currency_id}
                setCurrencyId={(currency_id)=>{
                    setPreference({...preference, currency_id});
                }}
                preferencesLoading={loading}
                preferencesError={error}
                userClubs={userClubs.map(e=>e.club_id)}
                setUserClubs={({id: club_id, checked})=>{
                    if(checked){
                        setUserClubs([...userClubs, {club_id, email}]);
                    }
                    else{
                        const _userClubs = [...userClubs];
                        const index = _userClubs.findIndex(e=>e.club_id===club_id);
                        if (index > -1) {
                            _userClubs.splice(index, 1);
                            setUserClubs(_userClubs);
                        }
                    }
                }}
                isAdmin={isAdmin}
                isComposer={isComposer}
                onAlertClick={()=>{
                    utils.redirect(history, "/compose");
                }}
                onAdminClick={()=>{
                    utils.refreshPage();
                }}
                onDiscardClick={()=>{
                    utils.refreshPage();
                }}
                onSaveClick={()=>{
                    onSaveClick({email, contact_medium_id, contact, userClubs, currency_id, theme_id, setIsSaving});
                }}
            />
        );
    }

};

export default PreferencesRoute;
