import PreviousMessages from '../components/PreviousMessages';
import usePreviousMessages from '../Hooks/usePreviousMessages';
import utils from '../utils';
import Loading from '../components/shared/Loading';

const PreviousMessagesRoute = ({history}) => {
    const [previousMessages, loading] = usePreviousMessages();

    if(loading){
        return <Loading />
    }
    else{
        return <PreviousMessages previousMessages={previousMessages} onBackClick={()=>{
            utils.redirect(history, "/compose");
        }}/>
    }
};

export default PreviousMessagesRoute;
