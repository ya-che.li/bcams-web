import {useContext, useState} from "react";
import ComposeMessage from '../components/ComposeMessage';
import useClubs from '../containers/ClubCheckboxListContainer/useClubs';
import utils from '../utils';
import {SENDER_ID} from '../config';
import useBillingFetch from '../Hooks/useBillingFetch';
import httpClient from '../Helpers/http-client';
import useFetchMessage from '../Hooks/useFetchMessage';
import {AuthContext} from '../providers/AuthProvider/AuthProvider';

const onSave = async ({bill: balance, header, priority_id, message, url, affected_club_ids, setIsSaving}) => {
    const payload = {
        sender_id: SENDER_ID,
        balance,
        header,
        priority_id,
        message,
        url,
        affected_club_ids
    };
    setIsSaving(true);
    await httpClient()
        .postAsync('messages/generate', payload)
        .then(async ({data}) => {
            let confirmMessage = `Recipients: ${data.recipients_count}\n\rTotal Cost: ${balance.currencySymbol}${data.total_cost}`;
            if (data.isLowBalance) {
                confirmMessage = `${confirmMessage}\n\rThe message can't be send due to insufficient funds.`;
                window.alert(confirmMessage);
                return;
            }
            confirmMessage = `Recipients: ${data.recipients_count}\n\rTotal Cost: $${data.total_cost}`;
            const confirm = window.confirm(`${confirmMessage}\n\rDo you really want to send this message ?`);

            if(confirm) {
                return await httpClient().postAsync('messages', data);
            }
        }).catch(error=>{
            console.error(error.message);
        }).finally(()=>{
            setIsSaving(false);
        });
};

const ComposeMessageRoute = ({history, match}) => {
    const {params:{id}} = match;
    const {email} = useContext(AuthContext);
    const [message, setMessage] = useFetchMessage(id);
    const {header, priority_id, message: description, url, affected_clubs=[]} = message;

    const [bill, is_balance_loading] = useBillingFetch(email);
    const [clubs, is_clubs_loading] = useClubs();

    const [is_global, setIsGlobal] = useState(false);
    const [is_saving, setIsSaving] = useState(false);

    const {balance, currencySymbol} = bill;
    const affected_club_ids = affected_clubs.map(e=>e.club_id);

    return (
		<ComposeMessage
            header={header}
            setHeader={header=>{
                setMessage({...message, header});
            }}
            priority_id={priority_id}
            setPriorityId={priority_id=>{
                setMessage({...message, priority_id});
            }}
            description={description}
            setDescription={description=>{
                setMessage({...message, message: description});
            }}
            url={url}
            setUrl={url=>{
                setMessage({...message, url});
            }}
            clubs={clubs}
            is_clubs_loading={is_clubs_loading}
            affected_club_ids={affected_club_ids}
            setAffectedClubIds={(club_ids=[])=>{
                const _affected_clubs = club_ids.map(club_id=>{
                    return {
                        message_id: id,
                        club_id
                    };
                });
                setMessage({ ...message, affected_clubs: _affected_clubs})
            }}
            is_global={is_global}
            setIsGlobal={setIsGlobal}
            is_balance_loading={is_balance_loading}
            currencySymbol={currencySymbol}
            balance={balance}
            is_saving={is_saving}
            onPreviousMessageClick={()=>{
                utils.redirect(history, "/compose/previous");
            }}
            onCancelClick={()=>{
                utils.redirect(history, "/");
            }}
            onSendClick={()=>{
                onSave({bill, header, priority_id, message: description, url, affected_club_ids, setIsSaving});
            }}
        />
	);
};

export default ComposeMessageRoute;
