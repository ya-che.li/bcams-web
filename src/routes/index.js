import PreferencesRoute from './PreferencesRoute';
import ComposeMessageRoute from './ComposeMessageRoute';
import PreviousMessagesRoute from './PreviousMessagesRoute';
import PreviousMessageDetailRoute from './PreviousMessageDetailRoute';
import ErrorPage from '../components/ErrorPage';

export default [
    {
        path: "/",
        component: PreferencesRoute,
        headerTitle: "Preferences"
    },
    {
        path: "/compose",
        component: ComposeMessageRoute,
        headerTitle: "Compose Messages"
    },
    {
        path: "/compose/previous",
        component: PreviousMessagesRoute,
        headerTitle: "Previous Messages"
    },
    {
        path: "/compose/previous/:id",
        component: PreviousMessageDetailRoute,
        headerTitle: "Resend Message"
    },
    {
        path: "/compose/:id",
        component: ComposeMessageRoute,
        headerTitle: "Compose Messages"
    },
    {
        path: "/error/:status/:message?",
        component: ErrorPage,
        headerTitle: "Error"
    }
];
