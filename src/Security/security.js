import React from "react";
import {ImplicitCallback, Security as OktaSecurity} from "@okta/okta-react";

const settings = {
  issuer: process.env.REACT_APP_OKTA_ISSUER,
  //issuer: 'https://dev-912316.okta.com/oauth2/default',
  redirect_uri: `${window.location.origin}/implicit/callback`,
  
  client_id: process.env.REACT_APP_OKTA_CLIENT_ID
  //client_id: '0oa2l6jfsqt2IBhia357'
};



const Security = ({ children }) =>  (
  <OktaSecurity {...settings}>{children}</OktaSecurity>
);

export {
	ImplicitCallback,
	Security
};
