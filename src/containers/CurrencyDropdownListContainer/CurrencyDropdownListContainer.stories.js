import {useState} from "react";
import {withKnobs} from "@storybook/addon-knobs";
import Container from './CurrencyDropdownListContainer';
import hooks from '../../Hooks';
import {useStub} from '../../../.storybook/hooks/useStub';

export default {
    title: "Container",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const CurrencyDropdownListContainer = () => {
    const data = [{id: 1, currency: "text1"}, {id: 2, currency: "text2"}];
    const [isStubbing] = useStub(hooks, "useAjax", {data, loading: false});
    const [value, setValue] = useState(1);

    if(isStubbing) return <div>Waiting...</div>;

    return <Container value={value} onChange={setValue} />
};
