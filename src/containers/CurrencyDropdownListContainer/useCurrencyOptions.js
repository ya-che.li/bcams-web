import hooks from '../../Hooks';

function map({id: value, currency: text}){
    return {value, text};
}

function useCurrencyOptions(){
    const {data:options, loading} = hooks.useAjax('currencies', []);

    return [options.map(map), loading];
}

export default useCurrencyOptions;
