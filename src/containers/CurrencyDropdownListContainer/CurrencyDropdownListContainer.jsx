import React from 'react';
import PropTypes from 'prop-types';
import useCurrencyOptions from './useCurrencyOptions';
import DropdownList from '../../components/shared/DropdownList/DropdownList';
import Loading from '../../components/shared/Loading';

const CurrencyDropdownListContainer = ({ test_id, value, onChange }) => {
    const [options, loading] = useCurrencyOptions();

    if(loading){
        return <Loading/>;
    }
    else{
        return <DropdownList
            test_id={test_id}
            options={options}
            loading={loading}
            placeholder="Currency"
            value={value}
            onChange={onChange}
        />
    }
};

export default CurrencyDropdownListContainer;

CurrencyDropdownListContainer.defaultProps = {
    test_id: "",
    onChange: ()=>{}
};

CurrencyDropdownListContainer.propTypes = {
    test_id: PropTypes.string,
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func
};
