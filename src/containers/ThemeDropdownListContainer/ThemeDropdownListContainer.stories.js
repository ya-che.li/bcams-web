import {useState} from "react";
import {withKnobs} from "@storybook/addon-knobs";
import Container from './ThemeDropdownListContainer';
import hooks from '../../Hooks';
import {useStub} from '../../../.storybook/hooks/useStub';

export default {
    title: "Container",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const ThemeDropdownListContainer = () => {
    const data = [{id: 1, colour: "Theme 1"}, {id: 2, colour: "Theme 2"}];
    const [isStubbing] = useStub(hooks, "useAjax", {data, loading: false});
    const [value, setValue] = useState(1);

    if(isStubbing) return <div>Waiting...</div>;

    return <Container value={value} onChange={setValue} />
};
