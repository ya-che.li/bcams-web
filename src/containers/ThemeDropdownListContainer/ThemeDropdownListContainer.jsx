import React from 'react';
import PropTypes from 'prop-types';
import useThemeOptions from './useThemeOptions';
import DropdownList from '../../components/shared/DropdownList/DropdownList';
import Loading from '../../components/shared/Loading';

const ThemeDropdownListContainer = ({ test_id, value, onChange }) => {
    const [options, loading] = useThemeOptions();

    if(loading){
        return <Loading />
    }

    return <DropdownList
        test_id={test_id}
        options={options}
        loading={loading}
        placeholder="theme"
        value={value}
        onChange={onChange}
    />
};

export default ThemeDropdownListContainer;

ThemeDropdownListContainer.defaultProps = {
    test_id: "",
    onChange: ()=>{}
};

ThemeDropdownListContainer.propTypes = {
    test_id: PropTypes.string,
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func
};
