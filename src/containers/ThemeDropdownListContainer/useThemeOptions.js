import hooks from '../../Hooks';

function map({id: value, colour: text}){
    return { value, text };
}

function useThemeOptions(){
    const {data: options, loading} = hooks.useAjax("themes", []);

    return [options.map(map), loading];
}

export default useThemeOptions;
