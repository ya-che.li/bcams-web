import hooks from '../../Hooks';

function map({id: value, name: text}){
    return { value, text };
}

function useContactOptions() {
    const {data: options, loading} = hooks.useAjax('contact-medium', []);

    return [options.map(map), loading];
}

export default useContactOptions;
