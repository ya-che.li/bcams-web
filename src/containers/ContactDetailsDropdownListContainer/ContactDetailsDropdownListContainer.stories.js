import {useState} from "react";
import {withKnobs} from "@storybook/addon-knobs";
import Container from './ContactDetailsDropdownListContainer';
import {useStub} from '../../../.storybook/hooks/useStub';
import hooks from '../../Hooks';

export default {
    title: "Container",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const ContactDetailsDropdownListContainer = () => {
    const data = [{id: 1, name: "text1"}, {id: 2, name: "text2"}];
    const [isStubbing] = useStub(hooks, "useAjax", {data, loading: false, error: ""});
    const [value, setValue] = useState(1);

    if(isStubbing) return <div>Waiting...</div>;

    return <Container value={value} onChange={setValue} />
};
