import React from 'react';
import PropTypes from 'prop-types';
import useContactOptions from './useContactOptions';
import DropdownList from '../../components/shared/DropdownList/DropdownList';
import Loading from '../../components/shared/Loading';

const ContactDetailsDropdownListContainer = ({ value, onChange }) => {
    const [options, loading] = useContactOptions();

    if(loading){
        return <Loading/>;
    }
    else{
        return <DropdownList
            placeholder="Type"
            options={options}
            loading={loading}
            value={value}
            onChange={onChange}
        />
    }
};

export default ContactDetailsDropdownListContainer;

ContactDetailsDropdownListContainer.defaultProps = {
    onChange: ()=>{}
};

ContactDetailsDropdownListContainer.propTypes = {
    value: PropTypes.number,
    onChange: PropTypes.func
};
