import React, {Fragment} from "react";
import ClubCheckbox from "../components/ClubCheckbox";

export default ({ clubs, isDisabled, onSelection }) => {
	
	const onSelectionChange = ({ id, selected = false }) => {
		const index = clubs.findIndex(e => e.id === id);
		clubs[index].selected = selected;
		
		onSelection(clubs);
	}

	if (!clubs) {
		return <Fragment />;
	} 

	return (
		<div className="ui checkbox">
			<fieldset>
				<legend>Affected clubs</legend>
				{
					clubs.map((club) => {
						const key = `${club.id}_${club.name}`;
						return <ClubCheckbox key={key} id={key} club={club} onChange={onSelectionChange} isDisabled={isDisabled} />
					})
				}
			</fieldset>
		</div>
	)
}
