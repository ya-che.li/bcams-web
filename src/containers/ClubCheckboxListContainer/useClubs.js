import hooks from '../../Hooks';

function useClubs(){
    const {data, loading, error} = hooks.useAjax('clubs', []);

    return [data, loading, error];
}

export default useClubs;
