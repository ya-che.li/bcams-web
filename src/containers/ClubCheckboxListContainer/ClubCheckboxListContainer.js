import PropTypes from "prop-types";
import ClubCheckboxList from '../../components/ClubCheckboxList';
import Loading from '../../components/shared/Loading';
import useClubs from './useClubs';

export default function ClubCheckboxListContainer({values, onChange}){
    const [clubs, loading] = useClubs();

    if(loading){
        return <Loading/>;
    }
    else{
        return <ClubCheckboxList
            values={values}
            onChange={onChange}
            clubs={clubs}
            loading={loading}
        />
    }
};

ClubCheckboxListContainer.defaultProps = {
    values: [],
    onChange: ()=>{}
};

ClubCheckboxListContainer.propTypes = {
    values: PropTypes.array,
    onChange: PropTypes.func
};
