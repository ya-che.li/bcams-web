import {useState} from "react";
import {withKnobs} from "@storybook/addon-knobs";
import Container from './ClubCheckboxListContainer';
import hooks from '../../Hooks';
import {useStub} from '../../../.storybook/hooks/useStub';

export default {
    title: "Container",
    decorators: [withKnobs],
    parameters: {
        info: {
            inline: true
        }
    }
};

export const ClubCheckboxListContainer = () => {
    const data = [{id: "value1", name: "text1"}, {id: "value2", name: "text2"}];
    const [isStubbing] = useStub(hooks, "useAjax", {data, loading: false, error: ""});
    const [values, setValues] = useState(["value1"]);

    if(isStubbing) return <div>Waiting...</div>;

    return <Container values={values} onChange={(({id, checked})=>{
        if (checked) {
            setValues([...values, id]);
        }
        else{
            const _values = [...values];
            _values.splice(_values.indexOf(id), 1);
            setValues(_values);
        }
    })} />
};
