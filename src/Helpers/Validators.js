export function get_mobile_disabled_text(mobile = "") {

    if (mobile === "") {
        return "Please enter a contact number"
    }

    const mobileNumberRegex = /^(?:(?:00)?44|0)7(?:[45789]\d{2}|624)\d{6}$/;

    if (!mobileNumberRegex.test(mobile)) {
        return mobile + " is not a valid mobile number";
    }
    return "";
}


export function get_email_disabled_text(email = "") {

    if (email === "") {
        return "Please enter an email address"

    }

    const emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    if (!emailRegex.test(email)) {
        return email + " is not a valid email address";
    }
    return "";
}


//export default isMessageValid