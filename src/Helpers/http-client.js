
const baseURL = 'http://localhost:4100';

let httpHeaders = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};
const httpClient = (authToken) => {
    const client = {};

    client.getAsync = async (relativeUri) => { 
        return fetch(`${baseURL}/${relativeUri}`,{
            headers: {
              Authorization : `Bearer ${authToken}`
            }})
            .then(processResponseAsync);
    }

    client.postAsync = async (relativeUri, payload) => { 
        return fetch(`${baseURL}/${relativeUri}`, {
            method: 'POST',
            headers: {
                ...httpHeaders,
                Authorization : `Bearer ${authToken}`
            },
            body: JSON.stringify(payload)
        }).then(processResponseAsync);
    }

    const processResponseAsync = async (response) => {
        const json = await response.json();

        if (response.status !== 200) {
            throw new Error(`${response.status}: ${json.message}`);
        }

        return json;
    }

    return client;
}

export default httpClient;