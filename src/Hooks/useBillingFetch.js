import hooks from './';

const defaultBill = {
    balance: 0.00,
    currency: "USD",
    currencySymbol: "$"
};

export default function useBillingFetch(email){
    const {data, loading, error} = hooks.useAjax(`billing/${email}/projectbalance`, defaultBill);

    return [data, loading, error];
}
