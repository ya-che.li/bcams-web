import hooks from './';

export default function useUserClubs(email){
    const {data, setData} = hooks.useAjax(`user-club/${email}`, []);

    return [data, setData];
}
