import {useEffect, useState} from "react";
import axios from "axios";
import {API_URL} from '../config';
import utils from '../utils';

const CancelToken = axios.CancelToken;

function useAjax(url="", defaultData, isUseEffect=true){
    const [loading, setLoading] = useState(isUseEffect);
    const [data, setData] = useState(defaultData);
    const [error, setError] = useState();

    useEffect(() => {
        const source = CancelToken.source();
        if(isUseEffect){
            setLoading(true);
            axios.get(`${API_URL}/${url}`,{
                headers: {
                    Authorization : `Bearer ${utils.getAccessToken()}`
                },
                cancelToken: source.token
            })
                .then(e=>e.data)
                .then(e=>e.data)
                .then(data=>{
                    setData(data);
                })
                .catch(error=>{
                    setError(error);
                }).finally(e=>{
                setLoading(false);
            });
        }
        return function cleanup(){
            source.cancel();
        };
    }, [url, isUseEffect]);

    return {data, setData, loading, error};
}

export default useAjax;
