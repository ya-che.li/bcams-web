import hooks from './';

function map({id, sent_date, priority_id, header, message, cost, recipients_count}){
    return {
        id,
        date: sent_date,
        priority: priority_id,
        header,
        message,
        recipients: recipients_count,
        cost
    }
}
function usePreviousMessages(){
    const {data, loading, error} = hooks.useAjax("messages", []);

    return [data.map(map), loading, error];
}

export default usePreviousMessages;
