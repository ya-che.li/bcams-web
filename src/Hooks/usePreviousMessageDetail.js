import hooks from './';

const usePreviousMessageDetail = (id) => {
    const {data, loading} = hooks.useAjax(`messages/${id}/sent`, []);

    return [data, loading];
};

export default usePreviousMessageDetail;
