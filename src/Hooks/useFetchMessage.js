import hooks from './';

const defaultMessage = {
    header: "",
    message: "",
    url: "",
    priority_id: "",
    affected_clubs: []
};

export default function useFetchMessage(id=""){
    const isEdit = id !== "";

    const {data: message, setData: setMessage, loading ,error} = hooks.useAjax(`messages/${id}`,defaultMessage, isEdit);

    return [
        message,
        setMessage,
        loading, 
        error
    ];
};
