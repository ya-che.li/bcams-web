import {useEffect, useState} from 'react';
import {contactMediums} from '../components/ContactDropdown';
import httpClient from '../Helpers/http-client';
import {currencies} from '../components/CurrencyDropdown';

export const usePreferences = (email,authToken) => {
    const [contact, setContact] = useState('');
    const [contactMediumId, setContactMediumId] = useState(contactMediums.sms);
    const [currencyId, setCurrencyId] = useState(currencies.usd) 
    const [theme_id, setThemeId] = useState('')  
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(false);

    useEffect(() => 
    {    
        if(!authToken || !email)
            return; 
        setLoading(true); 
        httpClient(authToken).getAsync(`preferences/${email}`)
            .then(result => {     
                setContact(result.data.contact);
                setContactMediumId(result.data.contact_medium_id);
                setCurrencyId(result.data.currency_id);
                setThemeId(result.data.theme_id); 
            })  
            .catch((e) => { 
                setError(e);  
            })
            .finally(() => {
                setLoading(false);
            });
    }, [email,authToken])

    return [ 
        contact, 
        setContact, 
        contactMediumId, 
        setContactMediumId, 
        currencyId, 
        setCurrencyId, 
        loading, 
        error,
        theme_id, 
        setThemeId 
    ];
}

export const useClubPreferences = (clubs, email,authToken) => {
    const [userClubs, setUserClubs] = useState([]);
    const [isUserAssociatedToClub, setIsUserAssociatedToClub] = useState(true);

    useEffect(() => { 
        function getUserClubPreferences(authToken) {
            fetchClubPreferences(email, authToken).then(result => {

                if (result.data === undefined || result.data.length === 0) {
                    setIsUserAssociatedToClub(false);
                }
                result.data.forEach(userClub => {
                    const tempClub = clubs.find((club) => { 
                        return club.id === userClub.club_id;
                    }); 

                    if (!tempClub) return;

                    tempClub.selected = true;
                });
                setUserClubs(clubs);
            })
        } 
        if(!authToken || !email)
            return;  
        getUserClubPreferences(authToken);
    }, [clubs, email,authToken]);
    
    return [userClubs, setUserClubs, isUserAssociatedToClub]; 
}

const fetchClubPreferences = async (email,authToken) => httpClient(authToken).getAsync(`user-club/${email}`);
