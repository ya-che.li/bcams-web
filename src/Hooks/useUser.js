import {ROLE_USER} from '../constants/roles';
import hooks from './';

const defaultValue = {
    roles: [{name: ROLE_USER}]
};

export default function useUser(email) {
    const {data, loading, error} = hooks.useAjax(`users/${email}`, defaultValue);

    return [ data, loading, error ];
};
