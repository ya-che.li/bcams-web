import {CONTACT_EMAIL} from '../constants/contacts';
import {DEFAULT_CURRENCY} from '../config';
import hooks from './';

const defaultPreference = {
    email: "",
    theme_id: -1,
    contact_medium_id: CONTACT_EMAIL,
    contact: "",
    currency_id: DEFAULT_CURRENCY
};

export default function usePreferences(email=""){
    const {data, setData, loading, error} = hooks.useAjax(`preferences/${email}`, defaultPreference);

    return [ data, setData, loading, error ];
};
