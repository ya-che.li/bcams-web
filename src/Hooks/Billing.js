import {useEffect, useState} from 'react';
import httpClient from '../Helpers/http-client';

const defaultBalance = {
    balance: 0.00,
    currency: "USD",
    currencySymbol: "$"
};
export const useBillingFetch = (email,authToken) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [balance, setBalance] = useState(defaultBalance);  
    
    useEffect(() => {
        if(!authToken || !email)
            return;   
        httpClient(authToken).getAsync(`billing/${email}/projectbalance`)
            .then(result => {  
                return(result.data); 
            })
            .then(result => { 
                setBalance(result) 
            }) 
            .catch((e) => {
                setError(e);
            });

        setLoading(false);
    }, [email,authToken]);

    return [balance, loading, error];
};
