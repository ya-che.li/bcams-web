import {useEffect, useState} from "react";
import utils from '../utils';
import {TEST_EMAIL, TEST_TOKEN} from '../config';

function useAuth(auth){
    const isTestingMode = utils.isTestingMode();
    const [token, setToken] = useState("");
    const [isLogin, setIsLogin] = useState(false);
    const [isAuthenticating, setIsAuthenticating] = useState(false);
    const [email, setEmail] = useState("");

    useEffect(() => {
        if(!isTestingMode){
            setIsAuthenticating(true);
            auth.isAuthenticated().then(isLogin=>{

                setIsAuthenticating(false);
                setIsLogin(isLogin);
                if(isLogin){
                    auth.getUser().then(user => {
                        setEmail(user.email)
                    });
                    auth.getAccessToken().then(token=> {
                        setToken(token);
                    });
                }
            });
        }
    }, [auth, token, isTestingMode]);

    if(isTestingMode){
        return {token: TEST_TOKEN, isLogin: true, isAuthenticating: false, email: TEST_EMAIL, auth: {logout:()=>{window.location.href = "/";}}};
    }
    else{
        return {token, isLogin, isAuthenticating, email, auth};
    }
}

export default useAuth;
