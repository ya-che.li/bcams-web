import {useEffect, useState} from 'react';
import httpClient from '../Helpers/http-client';

const mapClub = (club) => ({
    id: club.id,
    name: club.name,
    selected: false
});

export const useClubFetch = (authToken) => {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [clubs, setClubs] = useState([]);
    
    useEffect(() => { 
        if(!authToken)
            return; 
        httpClient(authToken).getAsync('clubs')
            .then(result => {
                let clubFetchRes = (result.data.map(mapClub)); 
                return clubFetchRes;
            })
            .then(results => setClubs(results))
            .catch((e) => {
                setError(e);
            });

        setLoading(false);
    }, [authToken]);

    return [clubs, setClubs, loading, error];
};
