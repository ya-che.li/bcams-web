import {darkTheme, defaultTheme, highContrastTheme} from "./themes";

export const DEFAULT_THEME = 401;
export const DARK_THEME = 402;
export const HIGH_CONTRAST_THEME = 403;

export default function chooseTheme(themeId = DEFAULT_THEME) {
  switch (themeId) {
    case DARK_THEME:
      return darkTheme;
    case HIGH_CONTRAST_THEME:
      return highContrastTheme;
    case DEFAULT_THEME:
      default:
          return defaultTheme;
  }
}
