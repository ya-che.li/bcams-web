export const darkTheme = {
  color: "#ebebeb",
  colorSecondary: "#333",
  headerBackground: "#303030",
  centralCardedcontent: "#3e3e3e",
  defaultButtonBackground: "#18191B",
  mainBackground: "#18191b",
  footerBackground: "#33322e"
};

export const defaultTheme = {
  color: "#ebebeb",
  colorSecondary: "#333",
  headerBackground: "#ff323c",
  centralCardedcontent: "#3e3e3e",
  defaultButtonBackground: "#18191B",
  mainBackground: "#18191b",
  footerBackground: "#33322e"
};

export const highContrastTheme = {
  color: "#ebebeb",
  colorSecondary: "#333",
  headerBackground: "#345A5E",
  centralCardedcontent: "#3e3e3e",
  defaultButtonBackground: "#18191B",
  mainBackground: "#18191b",
  footerBackground: "#305658"
};
