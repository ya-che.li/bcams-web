export const API_URL = 'http://localhost:4100';
export const MAX_TOTAL_MESSAGE_COUNT = 100;
export const SENDER_ID = "david.johnson@and.digital";
export const DEFAULT_CURRENCY = 1; //GBP
//issuer: 'https://dev-912316.okta.com/oauth2/default',
export const OKTA_ISSUER = process.env.REACT_APP_OKTA_ISSUER;
//client_id: '0oa2l6jfsqt2IBhia357'
export const OKTA_CLIENT_ID = process.env.REACT_APP_OKTA_CLIENT_ID;
export const TEST_TOKEN = "TEST_TOKEN";
export const TEST_EMAIL = "TEST_EMAIL@and.digital";
