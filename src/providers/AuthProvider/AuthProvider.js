import React from 'react';
import {withAuth} from '@okta/okta-react';
import useAuth from '../../Hooks/useAuth';
import FullScreenLoading from '../../components/shared/FullScreenLoading';

export const AuthContext = React.createContext([]);

function AuthProvider(props){
    const auth = useAuth(props.auth);

    if(auth.isAuthenticating || auth.email === ""){
        return <FullScreenLoading />
    }
    else{
        return <AuthContext.Provider value={auth}>
            {props.children}
        </AuthContext.Provider>
    }
}

export default withAuth(AuthProvider);
