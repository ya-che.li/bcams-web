import React, {useContext} from "react";
import usePreferences from '../../Hooks/usePreferences';
import FullScreenLoading from '../../components/shared/FullScreenLoading';
import {AuthContext} from '../AuthProvider/AuthProvider';

export const PreferencesContext = React.createContext({});

const PreferencesProvider = props => {
    const {email} = useContext(AuthContext);
    const preferenceHooksArray = usePreferences(email);

    const loading = preferenceHooksArray[2];
    if(loading){
        return <FullScreenLoading />
    }
    else{
        return (
            <PreferencesContext.Provider value={preferenceHooksArray}>
                {props.children}
            </PreferencesContext.Provider>
        );
    }
};

export default PreferencesProvider;
