import {TEST_EMAIL} from '../../src/config';
import {API_URL} from './config';
import preferences from './data/preferences';
import user from './data/user';
import currencies from './data/currencies';
import themes from './data/themes';

export function login() {
    window.localStorage.setItem("test", "true");
}

export function $(selector) {
    return cy.get(selector);
}

export function click(selector) {
    return $(selector).click();
}

export function css(selector, value) {
    return $(selector).should('have.css', value);
}

export function getAttribute(selector) {
    return $(selector).invoke("attr");
}

export function getText(selector) {
    return $(selector).invoke("text");
}

export function eq(actual="", expected=""){
    return actual.should('eq', expected);
}

export function shouldHaveText(selector, text) {
    $(selector).should("have.text", text);
}

export function shouldBeVisible(selector){
    $(selector).should("be.visible");
}

export function shouldNotBeVisible(selector){
    $(selector).should("not.be.visible");
}

export function openMockingServer() {
    cy.server();
}

export function closeMockingServer() {
    cy.server({enable: false});
}

export function mockUser() {
    cy.route(`${API_URL}/users/${TEST_EMAIL}`, user);
}

export function mockCurrencies() {
    cy.route(`${API_URL}/currencies`, currencies);
}

export function mockPreferences() {
    cy.route(`${API_URL}/preferences/${TEST_EMAIL}`, preferences);
}

export function mockThemes() {
    cy.route(`${API_URL}/themes`, themes);
}
