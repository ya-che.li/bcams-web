import {$, click, closeMockingServer, css, eq, getText, login, mockThemes, openMockingServer} from '../../utils';
import {WEB_URL} from '../../config';
import {
    themeList,
    themeListCurrentValue,
    themeListFirstOption,
    themeListSecondOption,
    themeListThirdOption,
    themePlaceholder
} from '../../pages/PreferencesPage';
import themes from '../../data/themes';
import {header} from '../../pages/MainLayout';

describe("Theme Dropdown", () => {
    beforeEach(() => {
        login();
        openMockingServer();
        mockThemes();
        cy.visit(WEB_URL);
    });

    afterEach(() => {
        closeMockingServer();
    });

    it("Test dropdown opens", () => {
        const themeDropdown = click(themeList);
        themeDropdown.should("have.class", "active");
    });

    it("The number of theme options is correct", () => {
        click(themeList);
        $(themeList).children().should("have.length", themes.data.length);
    });

    it("Check first option in the theme list is clickable", () => {
        click(themeList);
        click(themeListFirstOption);
        $(themeListCurrentValue).should("have.text", themes.data[0].colour);
    });

    it("Check the values in the theme list is in the same order", () => {
        getText(themePlaceholder).then(placeholder=>{
            const str = themes.data.map(e=>e.colour).join("");
            eq(getText(`${themeList} `), placeholder+ str);
        });
    });

    it("the Header color is different when a theme option is clicked", () => {
        click(themeList);
        click(themeListFirstOption);
        eq(css(header, "background-color"), 'rgb(255, 50, 60)');

        click(themeList);
        click(themeListSecondOption);
        eq(css(header, "background-color"), 'rgb(48, 48, 48)');

        click(themeList);
        click(themeListThirdOption);
        eq(css(header, "background-color"), 'rgb(52, 90, 94)');
    });
});
