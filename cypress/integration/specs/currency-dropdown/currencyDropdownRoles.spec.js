import {
    closeMockingServer,
    login,
    mockPreferences,
    openMockingServer,
    shouldBeVisible,
    shouldNotBeVisible
} from '../../utils';
import {API_URL, WEB_URL} from '../../config';
import {adminButton, alertButton, currencyField} from '../../pages/PreferencesPage';

describe("Currency Dropdown", ()=>{
    beforeEach(() => {
        login();
        openMockingServer();
        mockPreferences();
    });

    afterEach(()=>{
        closeMockingServer();
    });

    it("if the user is admin", ()=>{
        cy.route(`${API_URL}/users/*`, {"status":"success","statusCode":200,"data":{"email":"stephane.lobognon@and.digital","firstname":"Stephane","lastname":"Lobognon","roles":[{"id":50000,"name":"Admin"}]}});
        cy.visit(WEB_URL);
        shouldBeVisible(currencyField);
        shouldBeVisible(alertButton);
    });

    it("if the user is composer", ()=>{
        cy.route(`${API_URL}/users/*`, {"status":"success","statusCode":200,"data":{"email":"stephane.lobognon@and.digital","firstname":"Stephane","lastname":"Lobognon","roles":[{"id":50000,"name":"Composer"}]}});
        cy.visit(WEB_URL);
        shouldBeVisible(currencyField);
        shouldBeVisible(alertButton);
        shouldNotBeVisible(adminButton);
    });

    it("if the user is user", ()=>{
        cy.route(`${API_URL}/users/*`, {"status":"success","statusCode":200,"data":{"email":"stephane.lobognon@and.digital","firstname":"Stephane","lastname":"Lobognon","roles":[{"id":50000,"name":"User"}]}});
        cy.visit(WEB_URL);
        shouldNotBeVisible(currencyField);
        shouldNotBeVisible(alertButton);
        shouldNotBeVisible(adminButton);
    });
});
