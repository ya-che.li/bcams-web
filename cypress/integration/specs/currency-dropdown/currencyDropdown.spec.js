import {
    click,
    closeMockingServer,
    login,
    mockCurrencies,
    mockPreferences,
    mockUser,
    openMockingServer,
    shouldHaveText
} from '../../utils';
import {WEB_URL} from '../../config';
import {
    currencyList,
    currencyListCurrentValue,
    currencyListEUR,
    currencyListGBP,
    currencyListUSD
} from '../../pages/PreferencesPage';

describe('Currency Dropdown', () => {
    beforeEach(() => {
        login();
        openMockingServer();
        mockPreferences();
        mockUser();
        mockCurrencies();
        cy.visit(WEB_URL);
    });

    afterEach(() => {
        closeMockingServer();
    });

    it("Test dropdown opens", () => {
        const currencyDropdown = click(currencyList);
        currencyDropdown.should("have.class", "active")
    });

    it("Check GBP currency in dropdown", () => {
        click(currencyList); // Click dropdown
        click(currencyListGBP); //Select GBP on dropdown

        // Check that GBP is active in dropdown
        shouldHaveText(currencyListCurrentValue, "GBP");
    });

    it("Check USD currency in dropdown", () => {
        click(currencyList); // Click dropdown
        click(currencyListUSD); //Select USD on dropdown

        // Check that USD is active in dropdown
        shouldHaveText(currencyListCurrentValue, "USD");
    });

    it("Check EUR currency in dropdown", () => {
        click(currencyList); // Click dropdown
        click(currencyListEUR); //Select EUR on dropdown

        // Check that EUR is active in dropdown
        shouldHaveText(currencyListCurrentValue, "EUR");
    });
});
