const plugin = require( '@semantic-ui-react/craco-less');

module.exports = {
    plugins: [{ plugin }]
};
